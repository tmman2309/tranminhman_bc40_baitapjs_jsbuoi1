/** Ex 1
 * Đầu vào: Lương 1 ngày (100.000), số ngày làm việc (30)
 * 
 * Bước xử lý: Lương 1 ngày * Số ngày làm
 * 
 * Đầu ra: Tiền lương nhân viên = 3.000.000
 */

var luongngay = 100000;
var songaylamviec = 30;
var luong = null;

luong = luongngay * songaylamviec;
console.log("🚀 ~ file: index.js ~ line 14 ~ luong", luong);

/** Ex 2
 * Đầu vào: 5 số thực ~ 10 20 30 40 50
 * 
 * Bước xử lý: Tổng 5 số / 5
 * 
 * Đầu ra: GTTB (Giá trị trung bình) = 30
 */

var n1 = 10;
var n2 = 20;
var n3 = 30;
var n4 = 40;
var n5 = 50;
var GTTB = null;

GTTB = (n1 + n2 + n3 + n4 + n5)/5;
console.log("🚀 ~ file: index.js ~ line 32 ~ GTTB", GTTB);

/** Ex 3
 * Đầu vào: Giá USD (23.500), số tiền USD (2)
 * 
 * Bước xử lý: Giá USD * Số tiền USD
 * 
 * Đầu ra: Số tiền VNĐ = 47.000
 */

var usdPrice = 23500;
var usdQuantity = 2;
var vnd = null;

vnd = usdPrice * usdQuantity;
console.log("🚀 ~ file: index.js ~ line 47 ~ vnd", vnd);

/** Ex 4
 * Đầu vào: Chiều dài (5), chiều rộng (4)
 * 
 * Bước xử lý:
 *   Diện tích = chiều dài * chiều rộng
 *   Chu vi = (chiều dài + chiều rộng) * 2
 * 
 * Đầu ra: Diện tích = 20, Chu vi = 18
 */

var chieudai = 5;
var chieurong = 4;
var dientich, chuvi = null;

dientich = chieudai * chieurong;
console.log("🚀 ~ file: index.js ~ line 64 ~ dientich", dientich)

chuvi = (chieudai + chieurong) * 2;
console.log("🚀 ~ file: index.js ~ line 68 ~ chuvi", chuvi)

/** Ex 5
 * Đầu vào: 1 số có 2 chữ số (44)
 * 
 * Bước xử lý:
 *   donvi = 44 % 10
 *   chuc = Math.int(44/10)
 *   tong2kyso = donvi + chuc
 * 
 * Đầu ra: Tổng 2 ký số = 8
 */

var x = 44;
var donvi, chuc, tong2kyso = null;

donvi = 44 % 10;
chuc = Math.floor(44/10);

tong2kyso = donvi + chuc;
console.log("🚀 ~ file: index.js ~ line 86 ~ tong2kyso", tong2kyso)


